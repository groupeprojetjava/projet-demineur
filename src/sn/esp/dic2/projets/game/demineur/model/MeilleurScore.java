package sn.esp.dic2.projets.game.demineur.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;

import javax.swing.JOptionPane;

/**
 * Cette classe r�pr�sente un meilleur score.
 * 
 * @author Kenz
 *
 */
public class MeilleurScore implements Serializable, Comparator<MeilleurScore> {
	private static ArrayList<MeilleurScore> tabMeilleursScoreFacile;
	private static ArrayList<MeilleurScore> tabMeilleursScoreMoyen;
	private static ArrayList<MeilleurScore> tabMeilleursScoreDifficile;

	private int score;
	private int chrono;
	private String pseudo;
	private Date date;

	/**
	 * Constructeur par d�faut. Obligatoire pour la s�rialisation XML.
	 */
	public MeilleurScore() {
		// revoir si des fois attibu mankan dans l fichier
		score = 10;
		pseudo = null;
		chrono = 0;
	}

	public MeilleurScore(int score, int chrono, String pseudo) {
		this.score = score;
		this.pseudo = pseudo;
		this.chrono = chrono;
		this.date = new Date();
	}

	public static void initialiser() {
		tabMeilleursScoreFacile = (ArrayList<MeilleurScore>) XMLSerialisation
				.lire("meilleursScoreFacile.xml");
		tabMeilleursScoreMoyen = (ArrayList<MeilleurScore>) XMLSerialisation
				.lire("meilleursScoreMoyen.xml");
		tabMeilleursScoreDifficile = (ArrayList<MeilleurScore>) XMLSerialisation
				.lire("meilleursScoreDifficile.xml");
		if (tabMeilleursScoreFacile == null) {
			tabMeilleursScoreFacile = new ArrayList<MeilleurScore>();
		}
		if (tabMeilleursScoreMoyen == null) {
			tabMeilleursScoreMoyen = new ArrayList<MeilleurScore>();
		}
		if (tabMeilleursScoreDifficile == null) {
			tabMeilleursScoreDifficile = new ArrayList<MeilleurScore>();
		}

	}

	/**
	 * Permet de trier une liste des meilleurs scores. <br/>
	 * Seuls les 10 premiers vont �tre sauvegard�s.
	 * 
	 * @param lms
	 */
	public static void trier(ArrayList<MeilleurScore> lms) {

		Collections.sort(lms, new MeilleurScore());
		// maintenant on ne garde k les dix premiers Meilleurs score
		while (lms.size() > 10)
			lms.remove(10);

	}

	public static void addFacileScore(MeilleurScore ms) {
		// ajout le nouveau score
		tabMeilleursScoreFacile.add(ms);
		// trier pour avoir les dix premier
		trier(tabMeilleursScoreFacile);
		// les enregistrer dans le fichier correspondant
		XMLSerialisation.enregistrer("meilleursScoreFacile.xml",
				tabMeilleursScoreFacile);
		// JOptionPane.showMessageDialog(null, "ajouter au fichier");
	}

	public static void addMoyenScore(MeilleurScore ms) {
		// ajout le nouveau score
		tabMeilleursScoreMoyen.add(ms);
		// trier pour avoir les dix premier
		trier(tabMeilleursScoreMoyen);
		// les enregistrer dans le fichier correspondant
		XMLSerialisation.enregistrer("meilleursScoreMoyen.xml",
				tabMeilleursScoreMoyen);

	}

	public static void adddifficileScore(MeilleurScore ms) {
		// ajout le nouveau score
		tabMeilleursScoreDifficile.add(ms);
		// trier pour avoir les dix premier
		trier(tabMeilleursScoreDifficile);
		// les enregistrer dans le fichier correspondant
		XMLSerialisation.enregistrer("meilleursScoreDifficile.xml",
				tabMeilleursScoreDifficile);

	}

	public static String[] meilleursScoreFacile() {
		String[] lms = new String[100];
		int i = 0;
		for (Iterator iterator = tabMeilleursScoreFacile.iterator(); iterator
				.hasNext();) {
			MeilleurScore ms = (MeilleurScore) iterator.next();
			lms[i] = ms.toString();
			i++;

		}
		return lms;
	}

	public static String[] meilleursScoreMoyen() {

		String[] lms = new String[10];
		int i = 0;
		for (Iterator iterator = tabMeilleursScoreMoyen.iterator(); iterator
				.hasNext();) {
			MeilleurScore ms = (MeilleurScore) iterator.next();
			lms[i] = ms.toString();
			i++;

		}
		System.out.println("yepp meilleursScoreMoyen " + lms);
		return lms;
	}

	public static String[] meilleursScoreDifficile() {
		String[] lms = new String[10];
		int i = 0;
		for (Iterator iterator = tabMeilleursScoreDifficile.iterator(); iterator
				.hasNext();) {
			MeilleurScore ms = (MeilleurScore) iterator.next();
			lms[i] = ms.toString();
			i++;

		}
		return lms;
	}

	public int getscore() {
		return score;
	}

	public void setscore(int score) {
		this.score = score;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public static ArrayList<MeilleurScore> getTabMeilleursScoreFacile() {
		return tabMeilleursScoreFacile;
	}

	public static void setTabMeilleursScoreFacile(
			ArrayList<MeilleurScore> tabMeilleursScoreFacile) {
		MeilleurScore.tabMeilleursScoreFacile = tabMeilleursScoreFacile;
	}

	public static ArrayList<MeilleurScore> getTabMeilleursScoreMoyen() {
		return tabMeilleursScoreMoyen;
	}

	public static void setTabMeilleursScoreMoyen(
			ArrayList<MeilleurScore> tabMeilleursScoreMoyen) {
		MeilleurScore.tabMeilleursScoreMoyen = tabMeilleursScoreMoyen;
	}

	public static ArrayList<MeilleurScore> getTabMeilleursScoreDifficile() {
		return tabMeilleursScoreDifficile;
	}

	public static void setTabMeilleursScoreDifficile(
			ArrayList<MeilleurScore> tabMeilleursScoreDifficile) {
		MeilleurScore.tabMeilleursScoreDifficile = tabMeilleursScoreDifficile;
	}

	public int getChrono() {
		return chrono;
	}

	public void setChrono(int chrono) {
		this.chrono = chrono;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return " Score :" + score + ",en un temps chrono=" + chrono
				+ " secondes  par " + pseudo + " le " + date.toLocaleString();
	}

	@Override
	public int compare(MeilleurScore s1, MeilleurScore s2) {
		// tri desc
		if ((s1.chrono + s1.score) < (s2.chrono + s2.score)) {
			return -1;
		} else if ((s1.chrono + s1.score) > (s2.chrono + s2.score)) {
			return 1;
		} else {
			return 0;
		}
	}

}