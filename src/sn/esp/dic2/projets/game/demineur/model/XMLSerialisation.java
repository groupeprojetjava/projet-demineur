package sn.esp.dic2.projets.game.demineur.model;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
 
public class XMLSerialisation {
	public static void enregistrer(String fichier,Object obj){
		// Enregistrement de nos noeuds � partir de la racine
				try {
					XMLEncoder e = new XMLEncoder(
					        new BufferedOutputStream(
					            new FileOutputStream(fichier)));
					e.writeObject(obj);
					e.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
		
	}
	public static Object  lire(String fichier){
		Object obj=null;
		// Lecture (d�s�rialisation) de notre fichier XML
				try {
					XMLDecoder d = new XMLDecoder(new BufferedInputStream(
							new FileInputStream(fichier)));
					obj = (Object) d.readObject();
					d.close();
				} catch (FileNotFoundException e) {
					///e.printStackTrace();
					obj=null;
				}
				catch (Exception e) {
					///e.printStackTrace();
					obj=null;
				}
		return obj;
	}
	
 
}