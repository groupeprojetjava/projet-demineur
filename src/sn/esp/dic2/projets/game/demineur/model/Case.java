package sn.esp.dic2.projets.game.demineur.model;

public class Case implements Cloneable {
	private int positionX;
	private int positionY;

	public enum TypeEtat {
		vierge, decouverte, marquee, suspecte
	};

	private TypeEtat etat;
	private boolean mine;
	private Grille grille;

	/**
	 * Constructeur par d�faut. Obligatoire pour la s�rialisation XML.
	 */
	public Case() {
		// TODO Auto-generated constructor stub

	};

	private int poids;

	/**
	 * Constructucteur de la classe.
	 */
	public Case(int positionX, int positionY, boolean mine, Grille grille) {
		super();
		this.positionX = positionX;
		this.positionY = positionY;
		this.mine = mine;
		this.etat = TypeEtat.vierge;
		this.grille = grille;
	}

	/**
	 * Cette m�thode permet de d�couvrir une case.
	 */
	public void decouvrir() {
		this.etat = TypeEtat.decouverte;
		CalcPoids();
		// on change augmennte le score proportionnel au poids de la case
		this.getGrille().getModel()
				.setScore(this.getGrille().getModel().getScore() + 10 * poids);
		this.grille.getModel().setNbCaseDecouvertes(
				this.grille.getModel().getNbCaseDecouvertes() + 1);
		// on envoi le mise a jour a la vue
		this.grille.getModel().createModelChangeEvent(this);
		// si le poids est de zero on propage: on decouvre les cases adjacentes
		if (this.poids == 0) {
			propager();
		}

	}

	/**
	 * Cette m�thode permet de marquer une case.
	 */
	public void marquer() {
		switch (this.etat) {
		case vierge:
			// on marque la case
			this.etat = TypeEtat.marquee;
			// on incremente le nombre de cases marques
			this.grille.getModel().setNbCaseMarquees(
					this.grille.getModel().getNbCaseMarquees() + 1);
			break;
		case marquee:
			this.etat = TypeEtat.suspecte;
			break;
		case suspecte:
			// on enleve le marquage
			this.etat = TypeEtat.vierge;
			// on deremente l nombre de cases marquees
			this.grille.getModel().setNbCaseMarquees(
					this.grille.getModel().getNbCaseMarquees() - 1);
			break;
		default:
			break;
		}

		this.grille.getModel().createModelChangeEvent(this);
	}

	/**
	 * Cette m�thode permet de propager une case i.e. decouvrir les cases autour
	 * d'une case dont le poids est 0. <br/>
	 * Les cases contenant des mines sont ignor�es
	 */
	private void propager() {
		for (int i = this.positionX - 1; i <= this.positionX + 1; i++) {
			for (int j = this.positionY - 1; j <= this.positionY + 1; j++) {

				if (this.grille.getCase(i, j) != null
						&& this.grille.getCase(i, j).etat == TypeEtat.vierge) {
					this.grille.getCase(i, j).decouvrir();
				}
			}
		}

	}

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}

	public TypeEtat getEtat() {
		return etat;
	}

	public void setEtat(TypeEtat etat) {
		this.etat = etat;
	}

	public boolean isMine() {
		return mine;
	}

	public void setMine(boolean mine) {
		this.mine = mine;
	}

	public int getPoids() {
		return poids;
	}

	public void CalcPoids() {
		this.poids = 0;
		for (int i = this.positionX - 1; i <= this.positionX + 1; i++) {
			for (int j = this.positionY - 1; j <= this.positionY + 1; j++) {
				if (this.grille.getCase(i, j) != null) {
					if (this.grille.getCase(i, j).isMine())
						this.poids++;
				}

			}
		}
	}

	public Grille getGrille() {
		return grille;
	}

	public void setGrille(Grille grille) {
		this.grille = grille;
	}

	public void setPoids(int poids) {
		this.poids = poids;
	}

	public Case clone() {
		// System.out.println(this + "clone case");
		Case caseClone = null;
		try {
			caseClone = (Case) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// System.out.println(this + "clone case" + (caseClone != this)
		// + caseClone);
		return caseClone;
	}

}
