package sn.esp.dic2.projets.game.demineur.model;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.rmi.MarshalException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

import javax.swing.JOptionPane;

import sn.esp.dic2.projets.game.demineur.controller.ControllerChangeEvent;
import sn.esp.dic2.projets.game.demineur.controller.ControllerChangeListerner;
import sn.esp.dic2.projets.game.demineur.controller.ModelChangeEvent;
import sn.esp.dic2.projets.game.demineur.controller.ModelChangeListerner;

/**
 * Cette classe repr�sente le Mod�le du jeu.
 * 
 * @author darcia0001
 * 
 */
public class DemineurModel implements ControllerChangeListerner, Serializable,
		Cloneable {
	private int score;
	private int nbCaseDecouvertes;
	private int nbCaseMarquees;
	private Niveau niveau;
	private Grille grille = null;
	private Joueur joueur;
	ArrayList<ModelChangeListerner> listeners;

	public enum EtatPartie {
		enCours, gagne, perdu, pause
	};

	EtatPartie etat = EtatPartie.enCours;
	Compteur cpt;
	Stack<DemineurModel> prev;
	Stack<DemineurModel> next;

	/**
	 * Constructeur par d�faut. Obligatoire pour la s�rialisation XML
	 */
	public DemineurModel() {

	}

	/**
	 * construit le Demineur � partir du Mod�le.
	 * 
	 * @param niveau
	 */
	public DemineurModel(Niveau niveau) {
		super();
		this.niveau = niveau;
		this.score = 0;
		this.nbCaseDecouvertes = 0;
		this.nbCaseMarquees = 0;
		grille = new Grille(this.niveau.getNbLigne(),
				this.niveau.getNbColonne(), this);
		cpt = new Compteur();

		listeners = new ArrayList<ModelChangeListerner>();
		this.prev = new Stack<DemineurModel>();
		this.next = new Stack<DemineurModel>();
		joueur = new Joueur("Anon");

	}

	public void debuter() {

	}

	public void sauvegarder(String fichier) {
		XMLSerialisation.enregistrer(fichier, this);

	}

	/**
	 * Permet de charger un fichier XML dans lequel on avait sauvegard� une
	 * partie.
	 * 
	 * @param fichier
	 * @return
	 */
	public static DemineurModel ouvrir(String fichier) {
		DemineurModel d = (DemineurModel) XMLSerialisation.lire(fichier);
		return d;

	}

	/**
	 * Permet de mettre en pause une partie.
	 */
	public void pause() {
		System.out.println("paude\n");
		ModelChangeEvent mce = new ModelChangeEvent(this, -1, -1);
		mce.setScore(score);
		switch (this.etat) {
		case enCours:
			this.etat = EtatPartie.pause;
			mce.setEtatPartie(ModelChangeEvent.PAUSE);
			break;
		case pause:
			this.etat = EtatPartie.enCours;
			mce.setEtatPartie(ModelChangeEvent.ENCOURS);

			break;

		default:
			break;
		}
		this.fireModelChangeEvent(mce);
	}

	/**
	 * Permet d'annuler le dernier coup. <br/>
	 * Cette partie est g�r�e gr�ce � deux piles.<br/>
	 * Une pile pour les �tats pr�c�dents et une autre pour les �tats suivants.
	 */
	public void annuler() {
		if(this.etat==EtatPartie.perdu)return;//spartie perdu on revien plus;
		if(this.etat==EtatPartie.gagne)return;//spartie perdu on revien plus;
		DemineurModel d = null;
		System.out.println(score + "la case acteul"
				+ this.getGrille().getCase(0, 0));
		if (prev.size() > 0) {
			addEtatSuivant();
			d = prev.pop();
			System.out.println(d.score + "la case den la pile"
					+ d.getGrille().getCase(0, 0));
		}
		// s'il y a un etat precedant
		if (d != null) {
			System.out.println("annulation!!!!!" + prev.size());
			this.etat = d.getEtat();
			this.cpt = d.getCpt();
			this.grille = d.getGrille();
			this.listeners = d.getListeners();
			this.nbCaseDecouvertes = d.getNbCaseDecouvertes();
			this.nbCaseMarquees = d.getNbCaseMarquees();
			this.next = d.getNext();
			this.prev = d.getPrev();
			this.niveau = d.getNiveau();
			this.score = this.getScore();
			// on raffraichir la vue
			refreshView();
		}

	}

	/**
	 * Permet de ramener le jeu � l'�tat pr�cedant. Ceci est possible gr�ce aux
	 * piles. <br/>
	 * 
	 */
	public void retablir() {
		DemineurModel d = null;
		if (next.size() > 0) {
			addEtatPrecedant();
			d = next.pop();
		}
		// s(il y a un etat precedant
		if (d != null) {
			System.out.println("retablissement!!!!!" + prev.size());
			this.etat = d.getEtat();
			this.cpt = d.getCpt();
			this.grille = d.getGrille();
			this.listeners = d.getListeners();
			this.nbCaseDecouvertes = d.getNbCaseDecouvertes();
			this.nbCaseMarquees = d.getNbCaseMarquees();
			this.next = d.getNext();
			this.prev = d.getPrev();
			this.niveau = d.getNiveau();
			this.score = this.getScore();
			// on raffraichri la vue
			refreshView();
		}

	}

	@Override
	public String toString() {
		return "" + this.score;
	}

	/**
	 * Ajouter un etat dans la pile des �tats pr�c�dants.
	 */
	private void addEtatPrecedant() {
		// la taille ne doit jamais depasse 5
		if (prev.size() < 5) {
			prev.push(this.clone());
		} else {// au cas echeant eleve le plus ancien
			prev.remove(0);
			prev.push(this.clone());
		}
	};

	/**
	 * Ajouter un etat dans la pile des �tats suivants.
	 */
	private void addEtatSuivant() {
		// la taille ne doit jamais depasse 5
		if (next.size() < 5) {
			next.push(this.clone());
		} else {// au cas echeant eleve le plus ancien
			next.remove(0);
			next.push(this.clone());
		}
	};

	/**
	 * Permet d'actualiser la vue quand on annule ou r�tablit un coup.
	 */
	public void refreshView() {
		// on met a jour les
		for (int i = 0; i < this.grille.getNbLigne(); i++) {
			for (int j = 0; j < this.grille.getNbColonne(); j++) {
				this.grille.getCase(i, j).CalcPoids();
				createModelChangeEventSilent(this.grille.getCase(i, j));

			}
		}

	}

	@Override
	/**
	 * M�thode appell�e � chaque fois qu'un evenement ControllerChangeEvent est envoy�.
	 */
	public void controllerChanged(ControllerChangeEvent evt) {
		if (this.etat != EtatPartie.perdu && this.etat != EtatPartie.gagne) {
			// si la partie n'est pas deja termin�e on recupere la case
			// concern�e par l'event.
			Case laCase = grille.getCase(evt.getLigne(), evt.getColonne());
			if (evt.getEtat() == ControllerChangeEvent.REVELED&&laCase.getEtat()!=Case.TypeEtat.marquee&&laCase.getEtat()!=Case.TypeEtat.suspecte) {
				// on sauve l etat avant les modif
				this.addEtatPrecedant();
				laCase.decouvrir();

			} else if (evt.getEtat() == ControllerChangeEvent.MARKED) {
				// voir si c est la peine
				// on sauve l etat avant les modif
				this.addEtatPrecedant();
				laCase.marquer();
			}
		}

	}

	/**
	 * Permet de cr�er un l'objet ModelChangeEvent associ�e � la case et
	 * l'envoie aux �couteurs.
	 * 
	 * @param laCase
	 */
	public void createModelChangeEvent(Case laCase) {
		// creation de l event modelChangeEvent
		ModelChangeEvent evtModel = new ModelChangeEvent(laCase,
				laCase.getPositionX(), laCase.getPositionY());
		// parametrage d l evente ModelChangeevent
		switch (laCase.getEtat()) {
		case vierge:
			evtModel.setEtatCase(ModelChangeEvent.VIERGE);
			;
			break;
		case decouverte:
			evtModel.setEtatCase(ModelChangeEvent.REVELED);
			// on doit dans ce cas renseigner son poids
			evtModel.setPoids(laCase.getPoids());
			// si la case decouverte est minee
			if (laCase.isMine()) {
				if (this.etat == EtatPartie.perdu) {// si la partie est deja
													// terminer
					evtModel.setMine(true);// on ne fait que decouvrir une case
											// minee
				} else {// on change l etat de la partie a perdu et on decouvre
						// toute les case
					this.etat = EtatPartie.perdu;
					evtModel.setEtatPartie(ModelChangeEvent.PERDU);
					evtModel.setMine(true);
					grille.decouvrirToutesLesCasesMinees();
				}
			} else {
				/*
				 * si c'est la dernier case non min� decouverte on a gagne faut
				 * pas oublier que quand on perd toutes les cases sont
				 * decouvertes donc il faut exclure la decompte dans ce cas
				 */
				if (this.nbCaseDecouvertes == niveau.getNbColonne()
						* niveau.getNbLigne() - niveau.getNbMine()
						&& this.etat != EtatPartie.perdu) {
					evtModel.setEtatPartie(ModelChangeEvent.GAGNER);
					 //JOptionPane.showMessageDialog(null, "darcia gagner"+nbCaseDecouvertes);
					// on sauvegarde la partie si elle fait parti des meilleurs
					// score
					this.etat = EtatPartie.gagne;
					this.enregistrerMeilleurScore();
				}
			}

			break;
		case marquee:
			evtModel.setEtatCase(ModelChangeEvent.MARKED);
			break;
		case suspecte:
			evtModel.setEtatCase(ModelChangeEvent.SUSPECTED);
			break;
		default:
			break;
		}
		// on informe aussi du nouveau score et du nombre de mines restant
		evtModel.setScore(this.score);
		evtModel.setMineRestant(this.niveau.getNbMine() - nbCaseMarquees);
		System.out
				.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<vous avez gagne"
						+ this.nbCaseDecouvertes);
		this.fireModelChangeEvent(evtModel);

	}
	/**
	 * Permet de cr�er un l'objet ModelChangeEvent associ�e � la case et
	 * l'envoie aux �couteurs.
	 * 
	 * @param laCase
	 */
	public void createModelChangeEventSilent(Case laCase) {
		// creation de l event modelChangeEvent
		ModelChangeEvent evtModel = new ModelChangeEvent(laCase,
				laCase.getPositionX(), laCase.getPositionY());
		// parametrage d l evente ModelChangeevent
		evtModel.setModeSilent(true);
		switch (laCase.getEtat()) {
		case vierge:
			evtModel.setEtatCase(ModelChangeEvent.VIERGE);
			;
			break;
		case decouverte:
			evtModel.setEtatCase(ModelChangeEvent.REVELED);
			// on doit dans ce cas renseigner son poids
			evtModel.setPoids(laCase.getPoids());
			// si la case decouverte est minee
			if (laCase.isMine()) {
				if (this.etat == EtatPartie.perdu) {// si la partie est deja
													// terminer
					evtModel.setMine(true);// on ne fait que decouvrir une case
											// minee
				} else {// on change l etat de la partie a perdu et on decouvre
						// toute les case
					this.etat = EtatPartie.perdu;
					evtModel.setEtatPartie(ModelChangeEvent.PERDU);
					evtModel.setMine(true);
					grille.decouvrirToutesLesCasesMinees();
				}
			} else {
				/*
				 * si c'est la dernier case non min� decouverte on a gagne faut
				 * pas oublier que quand on perd toutes les cases sont
				 * decouvertes donc il faut exclure la decompte dans ce cas
				 */
				if (this.nbCaseDecouvertes == niveau.getNbColonne()
						* niveau.getNbLigne() - niveau.getNbMine()
						&& this.etat != EtatPartie.perdu) {
					evtModel.setEtatPartie(ModelChangeEvent.GAGNER);
					 JOptionPane.showMessageDialog(null, "darcia gagner"+nbCaseDecouvertes);
					// on sauvegarde la partie si elle fait parti des meilleurs
					// score
					this.etat = EtatPartie.gagne;
					this.enregistrerMeilleurScore();
				}
			}

			break;
		case marquee:
			evtModel.setEtatCase(ModelChangeEvent.MARKED);
			break;
		case suspecte:
			evtModel.setEtatCase(ModelChangeEvent.SUSPECTED);
			break;
		default:
			break;
		}
		// on informe aussi du nouveau score et du nombre de mines restant
		evtModel.setScore(this.score);
		evtModel.setMineRestant(this.niveau.getNbMine() - nbCaseMarquees);
		System.out
				.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<vous avez gagne"
						+ this.nbCaseDecouvertes);
		this.fireModelChangeEvent(evtModel);

	}

	public void addModelChangeListerner(ModelChangeListerner l) {
		this.listeners.add(l);
	}

	public void removeControllerChangeListerner(ModelChangeListerner l) {
		this.listeners.remove(l);
	}

	/**
	 * Envoie l'objet ModelChangeEvent pass� en param�tre aux �couteurs du
	 * Mod�le.
	 * 
	 * @param evt
	 */
	public void fireModelChangeEvent(ModelChangeEvent evt) {
		for (Iterator<ModelChangeListerner> iterator = this.listeners
				.iterator(); iterator.hasNext();) {
			ModelChangeListerner controller = (ModelChangeListerner) iterator
					.next();
			controller.modelChanged(evt);

		}
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getNbCaseDecouvertes() {
		return nbCaseDecouvertes;
	}

	public void setNbCaseDecouvertes(int nbCaseDecouvertes) {
		this.nbCaseDecouvertes = nbCaseDecouvertes;
	}

	public int getNbCaseMarquees() {
		return nbCaseMarquees;
	}

	public void setNbCaseMarquees(int nbCaseMarquees) {
		this.nbCaseMarquees = nbCaseMarquees;
	}

	public Niveau getNiveau() {
		return niveau;
	}

	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}

	public Compteur getCpt() {
		return cpt;
	}

	public void setCpt(Compteur cpt) {
		this.cpt = cpt;
	}

	public Grille getGrille() {
		return grille;
	}

	public void setGrille(Grille grille) {
		this.grille = grille;
	}

	public ArrayList<ModelChangeListerner> getListeners() {
		return listeners;
	}

	public void setListeners(ArrayList<ModelChangeListerner> listeners) {
		this.listeners = listeners;
	}

	public EtatPartie getEtat() {
		return etat;
	}

	public void setEtat(EtatPartie etat) {
		this.etat = etat;
	}

	public Stack<DemineurModel> getPrev() {
		return prev;
	}

	public void setPrev(Stack<DemineurModel> prev) {
		this.prev = prev;
	}

	public Stack<DemineurModel> getNext() {
		return next;
	}

	public void setNext(Stack<DemineurModel> next) {
		this.next = next;
	}

	/**
	 * Permet de faire le clonage de DemineurModel.
	 */
	public DemineurModel clone() {
		DemineurModel demClone = null;
		try {
			demClone = (DemineurModel) super.clone();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		demClone.setGrille(this.grille.clone());
		System.out.println("clone grille "
				+ (demClone.getGrille() != this.grille));
		System.out.println("et  grille.cases "
				+ (demClone.getGrille().getCases() != this.grille.getCases()));
		return demClone;
	}

	public void enregistrerMeilleurScore() {

		MeilleurScore ms = new MeilleurScore(this.score,
				this.cpt.getTimeEnSeconde(), this.joueur.getPseudo());
		MeilleurScore.initialiser();
		switch (niveau.getType()) {
		case Niveau.facile:
			MeilleurScore.addFacileScore(ms);
			break;
		case Niveau.moyen:
			MeilleurScore.addFacileScore(ms);
			break;
		case Niveau.difficile:
			MeilleurScore.addFacileScore(ms);
			break;

		default:
			break;
		}

	}

	public String[] getMeilleursScore() {
		MeilleurScore.initialiser();
		System.out.println("model soit meille score " + niveau.getType());
		switch (niveau.getType()) {
		case Niveau.facile:
			return MeilleurScore.meilleursScoreFacile();
		case Niveau.moyen:
			return MeilleurScore.meilleursScoreMoyen();
		case Niveau.difficile:
			return MeilleurScore.meilleursScoreDifficile();

		default:
			return null;

		}// switch

	}

	public void ChangerJoueur(String pseudo) {
		this.joueur.setPseudo(pseudo);

	}
}
