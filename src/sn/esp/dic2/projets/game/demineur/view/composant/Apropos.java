package sn.esp.dic2.projets.game.demineur.view.composant;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Apropos extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Apropos dialog = new Apropos();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Apropos() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("./image/Deminlogo64.png"));
		setBackground(Color.WHITE);
		setTitle("Apropos Demineur");
		setLocationRelativeTo(null);
	
		setBounds(100, 100, 773, 501);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("");
			lblNewLabel.setIcon(new ImageIcon("./image/Deminlogo642.png"));
			lblNewLabel.setBounds(55, 11, 620, 213);
			contentPanel.add(lblNewLabel);
		}
		
		JLabel lblLeJeuDemineur = new JLabel("<html><span>Demineur <br>Version: 1.0 release 1 <br>(c) Copyright DIC2 Info DGI ESP UCAD <br> contributeursrs and autres 2014, 2015. <br>Tout droits reserves.<br>Visiter <a href=\"google.com\">http://www.Demineur_Website.sn.com</a>/ </span></html>");
		lblLeJeuDemineur.setVerticalTextPosition(SwingConstants.TOP);
		lblLeJeuDemineur.setBounds(22, 279, 354, 98);
		contentPanel.add(lblLeJeuDemineur);
		
		JLabel lblLeMiniProjet = new JLabel("<html><span>Le jeu demineur ,coder a l occasion du projet de fin de module du langage java avancees a l ecole superierur polytechnique<br>le mini projet a ete r\u00E9aliser par un groupe d 'etudiant dynamique:Mamadou khoussa,Mamadou L. K. Kane,Khalifa Ababacar Ndiaye,Moustapha Seck</span></html>");
		lblLeMiniProjet.setBounds(22, 390, 735, 61);
		contentPanel.add(lblLeMiniProjet);
		
		JButton btnGuideDinstallation = new JButton("Guide d'installation");
		btnGuideDinstallation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Desktop desktop = null;
				java.net.URI url;
				try {
				url = new java.net.URI("http://localhost/Demineur_Website");
				if (Desktop.isDesktopSupported())
				{
				desktop = Desktop.getDesktop();
				desktop.browse(url);
				}
				}
				catch (Exception ex) {
				System.out.println(ex.getMessage());
				}
			}
		});
		btnGuideDinstallation.setBounds(524, 311, 170, 23);
		contentPanel.add(btnGuideDinstallation);
	}
}
