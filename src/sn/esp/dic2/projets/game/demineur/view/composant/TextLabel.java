package sn.esp.dic2.projets.game.demineur.view.composant;
 
import javax.swing.JLabel;

public class TextLabel extends JLabel {
	public static final  int chrono=0,score=1,mine=2;
	private int type;

	public TextLabel(String nom,int type) {
		super(nom);
		this.type=type;
		initialise();
	}

	 public void initialise(){
		 switch (type) {
		case TextLabel.chrono:
			this.setForeground(ThemeDemineur.getCouleurEcriture());
			this.setFont(ThemeDemineur.getFontText2());
			break;
		case TextLabel.score:
			this.setForeground(ThemeDemineur.getCouleurEcriture());
			this.setFont(ThemeDemineur.getFontnumber());
			break;
		case TextLabel.mine:
			this.setForeground(ThemeDemineur.getCouleurEcriture());
			this.setFont(ThemeDemineur.getFontnumber());
			break;

		default:
			break;
		}
	 }

}
