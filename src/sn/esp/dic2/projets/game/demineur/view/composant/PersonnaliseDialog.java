package sn.esp.dic2.projets.game.demineur.view.composant;

import java.awt.EventQueue;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;

import javax.swing.JSlider;

import java.awt.Insets;

import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dialog.ModalityType;
import java.awt.Font;

public class PersonnaliseDialog extends JDialog {
	private JTextField textFieldMine;
	private JTextField textFieldColonne;
	private JTextField textFieldLigne;
	private int nbMine=1,nbColonne=2,nbLigne=2;

 
	/**
	 * creation de la fenetre de  dialog.
	 */
	public PersonnaliseDialog() {
		
		this.setUndecorated(true);
		getContentPane(); 
		getContentPane().setFont(new Font("Microsoft YaHei", Font.PLAIN, 13));
		setModalityType(ModalityType.DOCUMENT_MODAL);
		setTitle("Personnaliser la grille");
		setLocationRelativeTo(null);
		setBounds(100, 100, 574, 349);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{91, 210, 11, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 105, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 26.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		JLabel lblNombreDeMine = new JLabel("Mines");
		GridBagConstraints gbc_lblNombreDeMine = new GridBagConstraints();
		gbc_lblNombreDeMine.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreDeMine.gridx = 0;
		gbc_lblNombreDeMine.gridy = 2;
		getContentPane().add(lblNombreDeMine, gbc_lblNombreDeMine);
		
		JSlider sliderMine = new JSlider();
		sliderMine.setValue(1);
		sliderMine.setMinimum(2);
		sliderMine.setMaximum(200);
		sliderMine.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider sld=(JSlider) arg0.getSource();
				textFieldMine.setText(""+sld.getValue());
				nbMine=sld.getValue();
			}
		});
		GridBagConstraints gbc_sliderMine = new GridBagConstraints();
		gbc_sliderMine.fill = GridBagConstraints.HORIZONTAL;
		gbc_sliderMine.insets = new Insets(0, 0, 5, 5);
		gbc_sliderMine.gridx = 1;
		gbc_sliderMine.gridy = 2;
		getContentPane().add(sliderMine, gbc_sliderMine);
		
		textFieldMine = new JTextField();
		textFieldMine.setEditable(false);
		textFieldMine.setText("1");
		GridBagConstraints gbc_textFieldMine = new GridBagConstraints();
		gbc_textFieldMine.insets = new Insets(0, 0, 5, 0);
		gbc_textFieldMine.gridx = 2;
		gbc_textFieldMine.gridy = 2;
		getContentPane().add(textFieldMine, gbc_textFieldMine);
		textFieldMine.setColumns(4);
		
		JLabel lblNombreDeColonnes = new JLabel("Colonnes");
		GridBagConstraints gbc_lblNombreDeColonnes = new GridBagConstraints();
		gbc_lblNombreDeColonnes.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreDeColonnes.gridx = 0;
		gbc_lblNombreDeColonnes.gridy = 4;
		getContentPane().add(lblNombreDeColonnes, gbc_lblNombreDeColonnes);
		
		JSlider sliderColonne = new JSlider();
		sliderColonne.setValue(2);
		sliderColonne.setMinimum(2);
		sliderColonne.setMaximum(50);
		sliderColonne.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider sld=(JSlider) arg0.getSource();
				textFieldColonne.setText(""+sld.getValue());
				nbColonne=sld.getValue();
			}
		});
		GridBagConstraints gbc_sliderColonne = new GridBagConstraints();
		gbc_sliderColonne.fill = GridBagConstraints.HORIZONTAL;
		gbc_sliderColonne.insets = new Insets(0, 0, 5, 5);
		gbc_sliderColonne.gridx = 1;
		gbc_sliderColonne.gridy = 4;
		getContentPane().add(sliderColonne, gbc_sliderColonne);
		
		textFieldColonne = new JTextField();
		textFieldColonne.setEditable(false);
		textFieldColonne.setText("2");
		GridBagConstraints gbc_textFieldColonne = new GridBagConstraints();
		gbc_textFieldColonne.insets = new Insets(0, 0, 5, 0);
		gbc_textFieldColonne.gridx = 2;
		gbc_textFieldColonne.gridy = 4;
		getContentPane().add(textFieldColonne, gbc_textFieldColonne);
		textFieldColonne.setColumns(4);
		
		JLabel lblNombreDeLignes = new JLabel("Lignes");
		GridBagConstraints gbc_lblNombreDeLignes = new GridBagConstraints();
		gbc_lblNombreDeLignes.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombreDeLignes.gridx = 0;
		gbc_lblNombreDeLignes.gridy = 6;
		getContentPane().add(lblNombreDeLignes, gbc_lblNombreDeLignes);
		
		JSlider sliderLigne = new JSlider();
		sliderLigne.setValue(2);
		sliderLigne.setMinimum(2);
		sliderLigne.setMaximum(50);
		sliderLigne.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider sld=(JSlider) arg0.getSource();
				textFieldLigne.setText(""+sld.getValue());
				nbLigne=sld.getValue();
			}
		});
		GridBagConstraints gbc_sliderLigne = new GridBagConstraints();
		gbc_sliderLigne.fill = GridBagConstraints.HORIZONTAL;
		gbc_sliderLigne.insets = new Insets(0, 0, 5, 5);
		gbc_sliderLigne.gridx = 1;
		gbc_sliderLigne.gridy = 6;
		getContentPane().add(sliderLigne, gbc_sliderLigne);
		
		textFieldLigne = new JTextField();
		textFieldLigne.setEditable(false);
		textFieldLigne.setText("2");
		GridBagConstraints gbc_textFieldLigne = new GridBagConstraints();
		gbc_textFieldLigne.insets = new Insets(0, 0, 5, 0);
		gbc_textFieldLigne.gridx = 2;
		gbc_textFieldLigne.gridy = 6;
		getContentPane().add(textFieldLigne, gbc_textFieldLigne);
		textFieldLigne.setColumns(4);
		
		JButton btnValider = new JButton("valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(nbMine>=(nbColonne*nbLigne)){
					JOptionPane.showMessageDialog(PersonnaliseDialog.this, "impossible que ne nombre de mines" +
							" soit sup�rieurc ou �gal au nombre de cases :"+(nbColonne*nbLigne), "erreur", 1);
				}else{
					PersonnaliseDialog.this.setVisible(false);
				}
				
			}
		});
		GridBagConstraints gbc_btnValider = new GridBagConstraints();
		gbc_btnValider.anchor = GridBagConstraints.SOUTHEAST;
		gbc_btnValider.insets = new Insets(0, 0, 0, 5);
		gbc_btnValider.gridx = 1;
		gbc_btnValider.gridy = 7;
		getContentPane().add(btnValider, gbc_btnValider);
		
		JButton btnAnnuler = new JButton("annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				PersonnaliseDialog.this.dispose();
			}
		});
		GridBagConstraints gbc_btnAnnuler = new GridBagConstraints();
		gbc_btnAnnuler.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAnnuler.anchor = GridBagConstraints.SOUTH;
		gbc_btnAnnuler.gridx = 2;
		gbc_btnAnnuler.gridy = 7;
		getContentPane().add(btnAnnuler, gbc_btnAnnuler);

	}
	public int[] getDonnees(){
		  int[] r= {nbMine,nbLigne,nbColonne};
		  return r;
	}
}
