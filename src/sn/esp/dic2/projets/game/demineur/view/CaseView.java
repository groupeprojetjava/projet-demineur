package sn.esp.dic2.projets.game.demineur.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.*;

import sn.esp.dic2.projets.game.demineur.controller.ControllerChangeEvent;
import sn.esp.dic2.projets.game.demineur.view.composant.ThemeDemineur;

public class CaseView extends JButton {
	DemineurView demineurView;
	private int positionX,positionY;
	private boolean marquee;
	private boolean revelee;
	ImageIcon imageIcon;
	
	public CaseView(){
		
	}
	public CaseView(DemineurView demineurView, int positionX, int positionY) {
		super("");
		setPreferredSize(new Dimension(64,64));		
		this.demineurView = demineurView;
		this.positionX = positionX; 
		this.positionY = positionY;
		this.marquee=false;
		this.revelee=false;
		this.addMouseListener(this.new CaseController() );
		this.setIcon(ThemeDemineur.getCaseViergeImage());
		
	}
	private class  CaseController extends MouseAdapter{
		public CaseController(){
			
		}
		@Override
		public void mouseClicked(MouseEvent eventMouse) {
			//on cree l objet event
			ControllerChangeEvent evt= new ControllerChangeEvent(eventMouse.getSource(), positionX, positionY);
			//paremetrage de l objet evenement suivant le type de clik
			if(eventMouse.getButton()==MouseEvent.BUTTON1){//tester l etat avant envoi
				evt.setEtat(ControllerChangeEvent.REVELED);
			}else
				if(eventMouse.getButton()==MouseEvent.BUTTON3){
					evt.setEtat(ControllerChangeEvent.MARKED);
			}
				
			// on envoi l evenement aux ecouteurs .
			demineurView.fireControllerChangeLister(evt);
		}
	}
	public DemineurView getDemineurView() {
		return demineurView;
	}
	public void setDemineurView(DemineurView demineurView) {
		this.demineurView = demineurView;
	}
	public int getPositionX() {
		return positionX;
	}
	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}
	public int getPositionY() {
		return positionY;
	}
	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}
	public boolean isMarquee() {
		return marquee;
	}
	public void setMarquee(boolean marquee) {
		this.marquee = marquee;
	}
	public boolean isRevelee() {
		return revelee;
	}
	public void setRevelee(boolean revelee) {
		this.revelee = revelee;
	}
	
	@Override
	public void setIcon(Icon defaultIcon) {
		// TODO Auto-generated method stub
		super.setIcon(defaultIcon);
		imageIcon=(ImageIcon) defaultIcon;
	}
	/**
	 * pour adapter l image a la taille de la case
	 */
	@Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        if(imageIcon!=null)
        g.drawImage(imageIcon.getImage(),0,0,getWidth(),getHeight(),this);
    }
}
