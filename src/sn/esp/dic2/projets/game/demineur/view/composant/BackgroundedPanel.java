package sn.esp.dic2.projets.game.demineur.view.composant;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;


public class BackgroundedPanel extends JPanel {
	static String BackgroundImage1;
	static String BackgroundImage2;
	
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            //ImageIcon m = new ImageIcon("./image/Light-Wood-Background-Wallpaper.jpg");
            ImageIcon m =  ThemeDemineur.getBackgroundImage();
            Image monImage = m.getImage();
            g.drawImage(monImage,0,0,getWidth(), getHeight(),this);
            //setBackground(new Color(152));
        }
   

}
