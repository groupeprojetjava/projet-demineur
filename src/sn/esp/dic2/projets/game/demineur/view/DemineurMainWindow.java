package sn.esp.dic2.projets.game.demineur.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.GridBagConstraints;
import java.io.IOException;
import java.util.Properties;

import javax.swing.*; 

import sn.esp.dic2.projets.game.demineur.controller.ControllerChangeEvent;
import sn.esp.dic2.projets.game.demineur.controller.ModelChangeEvent;
import sn.esp.dic2.projets.game.demineur.controller.ModelChangeListerner;
import sn.esp.dic2.projets.game.demineur.model.DemineurModel;
import sn.esp.dic2.projets.game.demineur.model.Niveau;
import sn.esp.dic2.projets.game.demineur.view.composant.Apropos;
import sn.esp.dic2.projets.game.demineur.view.composant.BackgroundedPanel;
import sn.esp.dic2.projets.game.demineur.view.composant.DemandePseudo;
import sn.esp.dic2.projets.game.demineur.view.composant.DemineuFileChooser;
import sn.esp.dic2.projets.game.demineur.view.composant.ImageResize;
import sn.esp.dic2.projets.game.demineur.view.composant.MenuButton;
import sn.esp.dic2.projets.game.demineur.view.composant.MyAudioPlayer;
import sn.esp.dic2.projets.game.demineur.view.composant.PersonnaliseDialog;
import sn.esp.dic2.projets.game.demineur.view.composant.TextLabel;
import sn.esp.dic2.projets.game.demineur.view.composant.ThemeDemineur;
/**
 * classe represantant la vue du demineur
 * @author darcia0001
 *
 */
public class DemineurMainWindow extends JFrame implements ActionListener,ModelChangeListerner {
	Niveau niv;
	DemineurModel demineurModel;
	DemineurView demineurView;
	String pseudo=null;
	
	
	// les deux panneaux du jeux
	private JPanel interfaceGame, interfaceParametre;
	private JPanel panParametre, tableauDebord, panMeilleursScores, cadre;
	// button pour definir les niveaux et les options de parametrage
	private JButton facile, moyen, difficile, personnalise,theme,son;
	// button pour changer de panneau
	private JButton btnAfficherPanneauParametrage, btnAfficherPanneauGame,btnMenu;
	// tableau de bord
	private JButton rejouer,annuler,retablir; 
	private JLabel imageLabel,scoreLabel,nbMineLabel;
	JLabel niveauLabel;
	//
	CardLayout pile;
	private JPanel panMenu;
	int delay = 1000;

	//horloge
	private Timer horloge; 
	private JLabel  horlogeLabel;
	//Jfile choooser pour enregistrer et ouvrir les fichier 
	private DemineuFileChooser manipulationFichier=new DemineuFileChooser();
	//joueur de son
	MyAudioPlayer thePlayer;
	public DemineurMainWindow() {
		// parametrage fenetre
		//cacher la barre de titre
         setUndecorated(true);
		setExtendedState(MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage("./image/Deminlogo64.png"));
		makeGUI();

		// instantiation du model
		niv = new Niveau(Niveau.facile);
		demineurModel = new DemineurModel(niv);
		// instantiation de la vue
		demineurView = new DemineurView(niv.getNbLigne(), niv.getNbColonne());
		cadre.add(demineurView); 

		// enregistrement des ecouteurs et des event controllerchangeevent
		// modelchangeevent
		demineurView.addControllerChangeListerner(demineurModel);

		demineurModel.addModelChangeListerner(this);//the main window ecoute aussi le model
		demineurModel.addModelChangeListerner(demineurView);
		horloge=new  Timer(delay, taskPerformer);
		horloge.start();
		nbMineLabel.setText("Mines: ");
		setVisible(true);
		repaint();
		//Son en background
		thePlayer = new MyAudioPlayer("./music/disiz.mp3", true);
	    thePlayer.start();


	}

	//qction listener du chrono ;etre
	ActionListener taskPerformer = new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			if (demineurModel.getCpt().getSecondes() == 60) {	
				demineurModel.getCpt().setSecondes(0) ;
				demineurModel.getCpt().minutesInc();
			}
			if (demineurModel.getCpt().getMinutes() == 60) {
				demineurModel.getCpt().setMinutes(0);
				demineurModel.getCpt().heuresInc();
			}
			demineurModel.getCpt().secondesInc();
			horlogeLabel.setText("" + demineurModel.getCpt().getHeures() + ":" + demineurModel.getCpt().getMinutes()+ ":" + demineurModel.getCpt().getSecondes());
		}
	};
	public void makeGUI(){


		// menu de l appication contenu dans un button 
		final JPopupMenu menuContenuDansUnButton = new JPopupMenu();
		JMenuItem changePseudo = new JMenuItem("Changer de joueur");
		JMenuItem ouvrirPartie = new JMenuItem("ouvrir une partie");
		JMenuItem enregistrePartie = new JMenuItem("Enregistrer la partie");
		JMenuItem quitter = new JMenuItem("Quitter");
		JMenuItem aide = new JMenuItem("Aide");
		JMenuItem apropos = new JMenuItem("A propos");
		JMenuItem changerSon = new JMenuItem("Charger un son");
		
		menuContenuDansUnButton.add(ouvrirPartie);
		menuContenuDansUnButton.add(enregistrePartie);
		menuContenuDansUnButton.add(changePseudo);
		menuContenuDansUnButton.add(changerSon);
		menuContenuDansUnButton.add(aide);
		menuContenuDansUnButton.add(apropos);
		menuContenuDansUnButton.add(quitter);

		/* 
		  quand on ouvre une partie on regenere le model et la vue
		  et pour chakk etat de case on rafrechi la vue
		 */
		ouvrirPartie.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				String filename= DemineurMainWindow.this.manipulationFichier.ouvrirFile();
				DemineurModel partieSauvegarder =DemineurModel.ouvrir(filename);
				if (partieSauvegarder!=null){
					               	//on detache  l instance de demineurView actif du cadre
					           		cadre.remove(demineurView);
					           		//on charge mla partie isssu du fichier de sauvegarde
					           		demineurModel=partieSauvegarder;
					               	niv=demineurModel.getNiveau(); 
					               	demineurView = new DemineurView(niv.getNbLigne(), niv.getNbColonne());
					               	demineurModel.addModelChangeListerner(demineurView);
					               	demineurModel.addModelChangeListerner(DemineurMainWindow.this);
					               	demineurView.addControllerChangeListerner(demineurModel);
					               	cadre.add(demineurView);
					               	demineurModel.refreshView();
					               	//
					               	cadre.repaint();
					           		cadre.revalidate();
				}else{
					JOptionPane.showMessageDialog(null, "le  fichier de sauvegarde "+filename+"  n'est valide");
				}

			}
		});
		enregistrePartie.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				String filename=DemineurMainWindow.this.manipulationFichier.enregistrerFile();
				//on a pas besoin de serialiser les listeners on peut toujour les regenerer a partir du model
				DemineurMainWindow.this.demineurModel.removeControllerChangeListerner(DemineurMainWindow.this.demineurView);
				DemineurMainWindow.this.demineurModel.removeControllerChangeListerner(DemineurMainWindow.this);
				DemineurMainWindow.this.demineurModel.sauvegarder(filename);
				//les remettre apres enregistrement
				DemineurMainWindow.this.demineurModel.addModelChangeListerner(DemineurMainWindow.this.demineurView);
				DemineurMainWindow.this.demineurModel.addModelChangeListerner(DemineurMainWindow.this);

			}
		});

		changePseudo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				DemandePseudo dp=new DemandePseudo();
				dp.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dp.setVisible(true);
				if(dp!=null){
					pseudo=dp.getDonnees();
					DemineurMainWindow.this.demineurModel.ChangerJoueur(pseudo);
					 
				}

			}
		});
		changerSon.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				String filename= DemineurMainWindow.this.manipulationFichier.ouvrirFile();
				if (filename!=null){
					thePlayer.close();
					thePlayer = new MyAudioPlayer(filename, true);
				    thePlayer.start();
				}else{
					JOptionPane.showMessageDialog(null, "le  fichier   son "+filename+"  n'est valide");
				}

			}
		});
		//kitter l application
		quitter.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				
			}
		});
		apropos.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				Apropos apropos=new Apropos();
				apropos.setVisible(true);
			}
		});
		aide.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Desktop desktop = null;
				java.net.URI url;
				try {
				url = new java.net.URI("http://localhost/Demineur_Website");
				if (Desktop.isDesktopSupported())
				{
				desktop = Desktop.getDesktop();
				desktop.browse(url);
				}
				}
				catch (Exception ex) {
				System.out.println(ex.getMessage());
				}
				
			}
		});
		// fin menu
		//pile des interfaces
		
		pile = new CardLayout();
		this.setLayout(pile);

		// interface gaming
		interfaceGame = new BackgroundedPanel();
		interfaceGame.setLayout(new BorderLayout());
		this.add(interfaceGame, "panGame");
		//ses elements
		//panneau menu
		//sdkljsdkl
		panMenu=new JPanel();
		panMenu.setLayout( new GridBagLayout());
		GridBagConstraints gbc=new GridBagConstraints();
		GridBagConstraints c=new GridBagConstraints();
		btnAfficherPanneauParametrage = new MenuButton("",MenuButton.menu);
		btnAfficherPanneauParametrage.setIcon(  ThemeDemineur.getIconParametre());
		btnAfficherPanneauParametrage.addActionListener(this);
		btnMenu=new MenuButton("menu",MenuButton.menu); 
		btnMenu.setIcon(ThemeDemineur.getIconMenu());
		niveauLabel=new JLabel("FACILE 9x9");niveauLabel.setFont(ThemeDemineur.getFontText3());niveauLabel.setForeground(ThemeDemineur.getCouleurEcriture());
		
		//pmlacement du btnAfficherPanneauParametrage
		gbc.gridx=1;
		gbc.gridy=0;
		gbc.fill = GridBagConstraints.BOTH;
		panMenu.add(btnAfficherPanneauParametrage,gbc);
		//pmlacement du niveauLabel
		gbc.gridx=2;
		gbc.gridy=0;
		gbc.fill = GridBagConstraints.BOTH;
		panMenu.add(niveauLabel,gbc);
		//placement du btnMenu
		c.fill = GridBagConstraints.HORIZONTAL;       
		c.weighty = 1.0;    
		c.anchor = GridBagConstraints.PAGE_END;  
		c.gridx = 0;        
		c.gridwidth = 2;   
		c.gridy = 1;        
		panMenu.add(btnMenu,c);
		//active le menu si on clik sur btnMenu
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				System.out.println("hellp");
				//		        menuContenuDansUnButton.show(btnMenu, btnMenu.getBounds().x, btnMenu.getBounds().y+btnMenu.getBounds().height);
				menuContenuDansUnButton.show(btnMenu,btnMenu.getBounds().x, -menuContenuDansUnButton.getHeight() );
			}
		});

		interfaceGame.add(panMenu, BorderLayout.WEST);
		tableauDebord = new JPanel();
		tableauDebord.setLayout(new GridLayout(9,1));
		tableauDebord.add( scoreLabel =new TextLabel("score:00",TextLabel.score));
		tableauDebord.add( imageLabel=new JLabel());
		imageLabel.setPreferredSize(new Dimension(100,100));
		imageLabel.setIcon(ThemeDemineur.getEmoticonJouer());
		tableauDebord.add(nbMineLabel= new TextLabel("00",TextLabel.mine));
		tableauDebord.add(horlogeLabel = new TextLabel("dd",TextLabel.chrono));
		tableauDebord.add(rejouer = new MenuButton("Rejouer",MenuButton.action));
		tableauDebord.add(annuler = new MenuButton("Annuler",MenuButton.action));
		tableauDebord.add(retablir = new MenuButton("Retablir",MenuButton.action));
		//les fonts
		 		scoreLabel.setFont(ThemeDemineur.getFontnumber());
		 		nbMineLabel.setFont(ThemeDemineur.getFontnumber());
		 		horlogeLabel.setFont(ThemeDemineur.getFontnumber());
		//les listener des button
		rejouer.addActionListener(this);
		annuler.addActionListener(this);
		retablir.addActionListener(this);
		imageLabel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent eventMouse) {
				DemineurMainWindow.this.demineurModel.pause();
			}
		});
		interfaceGame.add(tableauDebord, BorderLayout.EAST);
		cadre = new JPanel();
		cadre.setLayout(new GridBagLayout());

		interfaceGame.add(cadre, BorderLayout.CENTER);
		//rendre les element d l interface non opaquue pour faire ressortir le background
		panMenu.setOpaque(false);
		cadre.setOpaque(false);
		tableauDebord.setOpaque(false);

		// interface parametrage
		interfaceParametre = new BackgroundedPanel();
		interfaceParametre.setLayout(new BorderLayout());
		this.add(interfaceParametre, "panParametre");

		panParametre = new JPanel();
		panParametre.setLayout(new GridBagLayout());
		//

		GridBagConstraints c1=new GridBagConstraints();
		c1.fill = GridBagConstraints.NONE;
		c1.ipady = 0;       
		c1.weighty = 1.0;    
		c1.anchor = GridBagConstraints.PAGE_START;  
		c1.insets = new Insets(10,0,0,0);   
		c1.gridx = 0;        
		c1.gridwidth = 1;   
		c1.gridy = 0;     
		//
		JPanel btnsNiveau = new JPanel(); 
		btnsNiveau.setLayout(new GridLayout(6, 1));
		btnsNiveau.add(facile = new MenuButton("Facile",MenuButton.option));
		btnsNiveau.add(moyen = new MenuButton("Moyen",MenuButton.option));
		btnsNiveau.add(difficile = new MenuButton("Difficile",MenuButton.option));
		btnsNiveau.add(personnalise = new MenuButton("Personnalise",MenuButton.option));
		btnsNiveau.add(theme = new MenuButton("Theme",MenuButton.option));
		btnsNiveau.add(son = new MenuButton("Son",MenuButton.option));
		son.setIcon(ThemeDemineur.getIconSon());
		btnsNiveau.setAlignmentX(0.5f);
		//ajout de mainwindow comm  listener
		facile.addActionListener(this);//facile.setToolTipText("Niveau");
		moyen.addActionListener(this);
		difficile.addActionListener(this);
		personnalise.addActionListener(this);
		theme.addActionListener(this);
		son.addActionListener(this);
		
		//placement du btnAfficherPanneauGame
		gbc.gridx=1;
		gbc.gridy=0;
		gbc.fill = GridBagConstraints.BOTH;
		panParametre.add(btnAfficherPanneauGame = new MenuButton("<<",MenuButton.menu),gbc);
		//placement du btnsNiveau
				panParametre.add(btnsNiveau);

		btnAfficherPanneauGame.setIcon(ThemeDemineur.getIconGame());
		btnAfficherPanneauGame.addActionListener(this);
		interfaceParametre.add(panParametre, BorderLayout.WEST);
		panMeilleursScores = new JPanel();
		panMeilleursScores.add(new JLabel("Meilleurs scores"));
		interfaceParametre.add(panMeilleursScores, BorderLayout.CENTER);
		//opacit� des elements
		panParametre.setOpaque(false);
		panMeilleursScores.setOpaque(false);
		btnsNiveau.setOpaque(false);
		interfaceParametre.setOpaque(false);




	}
	public static void main(String[] arg) {

		DemineurMainWindow fenetre = new DemineurMainWindow();
	}

	private void initialiserGame(Niveau v) {
		cadre.remove(demineurView);
		// instanti� le niveau
		niv = v;
		// instanti� le model
		demineurModel = new DemineurModel(niv);
		if(pseudo!=null){demineurModel.ChangerJoueur(pseudo);}
		// instantiation de la vue
		demineurView = new DemineurView(niv.getNbLigne(), niv.getNbColonne());
		cadre.add(demineurView);
		/* enregistrement des ecouteurs et des event controllerchangeevent
		 et modelchangeevent*/
		demineurView.addControllerChangeListerner(demineurModel);
		demineurModel.addModelChangeListerner(this);//the main window ecoute aussi le model
		demineurModel.addModelChangeListerner(demineurView);
		//
		scoreLabel.setText("score:0");
		imageLabel.setIcon(ThemeDemineur.getEmoticonJouer());
		cadre.repaint();
		cadre.revalidate();
	}

	@SuppressWarnings("unused")
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() instanceof JButton) { 
			JButton src = (JButton) arg0.getSource();
			if (src == btnAfficherPanneauParametrage) {
				pile.next(this.getContentPane());
			} else if (src == btnAfficherPanneauGame) {
				pile.first(this.getContentPane());
				//on continu la decompte
				horloge.start();
			} else if (src == facile) {
				Niveau niveau=new Niveau(Niveau.facile);
				initialiserGame(niveau);
				niveauLabel.setText("Facile "+niveau.getNbLigne()+"x"+niveau.getNbColonne());
				afficherMeilleursScores("Facile");
				//on montre la selection
				facile.setSelected(true);
				moyen.setSelected(false);
				difficile.setSelected(false);
				personnalise.setSelected(false);

			} else if (src == moyen) {
				Niveau niveau=new Niveau(Niveau.moyen);
				initialiserGame(niveau);
				afficherMeilleursScores("Moyen");
				niveauLabel.setText("Moyen "+niveau.getNbLigne()+"x"+niveau.getNbColonne());
				//on montre la selection
				facile.setSelected(false);
				moyen.setSelected(true);
				difficile.setSelected(false);
				personnalise.setSelected(false);

			} else if (src == difficile) {
				Niveau niveau=new Niveau(Niveau.difficile);
				initialiserGame(niveau);
				demineurView.setPreferredSize(new Dimension(900, 700));
				niveauLabel.setText("Difficile "+niveau.getNbLigne()+"x"+niveau.getNbColonne());
				afficherMeilleursScores("Difficile");
				//on montre la selection
				facile.setSelected(false);
				moyen.setSelected(false);
				difficile.setSelected(true);
				personnalise.setSelected(false);

			} else if (src == personnalise) {
				PersonnaliseDialog perso=new PersonnaliseDialog();
				perso.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				perso.setVisible(true);
				if(perso!=null){
					int[] r =perso.getDonnees();
					initialiserGame(new Niveau(r[1], r[2], r[0]));
					if(r[1]>30||r[2]>30){
						demineurView.setPreferredSize(new Dimension(900, 700));
					}
					afficherMeilleursScores("Personnalise");
					niveauLabel.setText("Personnalise "+r[1]+"x"+r[2]);
				}else{
					Niveau niveau=new Niveau(Niveau.facile);
					initialiserGame(niveau);
					niveauLabel.setText("Facile "+niveau.getNbLigne()+"x"+niveau.getNbColonne());
				}	
				//on montre la selection
				facile.setSelected(false);
				moyen.setSelected(false);
				difficile.setSelected(false);
				personnalise.setSelected(true);

			}
			else if (src == theme) {
				panMeilleursScores.removeAll();panMeilleursScores.setOpaque(false);
				panMeilleursScores.setLayout(new GridLayout(3, 2,20,20));
				
				for (int i = 0; i < ThemeDemineur.getNbTheme(); i++) {
					//pour le transmettre dans le listener
					final int  positionTheme=i;
					JButton  illustration =new ImageResize( ThemeDemineur.getThemeImage(i) );
					illustration.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent eventMouse) {
							 if(eventMouse.getButton()==MouseEvent.BUTTON1){//click gauche
								ThemeDemineur.setTheme(positionTheme);
								ThemeDemineur.initialiseCaseImage();
								niveauLabel.setBackground(ThemeDemineur.getCouleur());
								niveauLabel.setForeground(ThemeDemineur.getCouleurEcriture());
								rejouer.setBackground(ThemeDemineur.getCouleur());
								rejouer.setForeground(ThemeDemineur.getCouleurEcriture());
								annuler.setBackground(ThemeDemineur.getCouleur());
								annuler.setForeground(ThemeDemineur.getCouleurEcriture());
								retablir.setBackground(ThemeDemineur.getCouleur());
								retablir.setForeground(ThemeDemineur.getCouleurEcriture());
								scoreLabel.setBackground(ThemeDemineur.getCouleur());
								scoreLabel.setForeground(ThemeDemineur.getCouleurEcriture());
								facile.setBackground(ThemeDemineur.getCouleur());
								facile.setForeground(ThemeDemineur.getCouleurEcriture());
								moyen.setBackground(ThemeDemineur.getCouleur());
								moyen.setForeground(ThemeDemineur.getCouleurEcriture());
								difficile.setBackground(ThemeDemineur.getCouleur());
								difficile.setForeground(ThemeDemineur.getCouleurEcriture());
								personnalise.setBackground(ThemeDemineur.getCouleur());
								personnalise.setForeground(ThemeDemineur.getCouleurEcriture());
								son.setBackground(ThemeDemineur.getCouleur());
								son.setForeground(ThemeDemineur.getCouleurEcriture());
								theme.setBackground(ThemeDemineur.getCouleur());
								theme.setForeground(ThemeDemineur.getCouleurEcriture());
								btnAfficherPanneauGame.setBackground(ThemeDemineur.getCouleur());
								btnAfficherPanneauGame.setForeground(ThemeDemineur.getCouleurEcriture());
								btnAfficherPanneauParametrage.setBackground(ThemeDemineur.getCouleur());
								btnAfficherPanneauParametrage.setForeground(ThemeDemineur.getCouleurEcriture());
								nbMineLabel.setBackground(ThemeDemineur.getCouleur());
								nbMineLabel.setForeground(ThemeDemineur.getCouleurEcriture());
								horlogeLabel.setBackground(ThemeDemineur.getCouleur());
								horlogeLabel.setForeground(ThemeDemineur.getCouleurEcriture());
								btnMenu.setIcon(ThemeDemineur.getIconMenu());;
								btnAfficherPanneauGame.setIcon(ThemeDemineur.getIconGame());
								btnAfficherPanneauParametrage.setIcon(ThemeDemineur.getIconParametre());;
								DemineurMainWindow.this.demineurModel.refreshView();
								repaint();
							}
						}
					});//listener
					panMeilleursScores.add(illustration);
				}//for
				repaint();
				 
			}
			else if (src == son) {
				if(MyAudioPlayer.mute==false){
					MyAudioPlayer.mute=true;
					thePlayer.close();
					son.setIcon(ThemeDemineur.getIconMute());
				}else{
					MyAudioPlayer.mute=false;
					son.setIcon(ThemeDemineur.getIconSon());
				}
				
			}else if (src == rejouer) {
				initialiserGame(niv );
				imageLabel.setIcon(ThemeDemineur.getEmoticonJouer());
				horloge.stop();
				horloge.start();
			}else if (src == annuler) {
				demineurModel.annuler();

			}else if (src == retablir) {
				demineurModel.retablir();
			}


		}//grand if

	}//action performed
	/**
	 * methode ki a chak niveau va afficher dans le panneau panMeilleursScores 
	 *
	 * la liste des 5 meilleurs scores et la date
	 *ainsi k les info concernant le niveau
	 */
	public void afficherMeilleursScores(String nomNiveau){
		JPanel panTitre = new  JPanel();
		//panTitre.setPreferredSize(new Dimension(1));
		JLabel descNiveauLabel = new javax.swing.JLabel();
		descNiveauLabel.setText("Niveau "+nomNiveau+" :Grillle de "+niv.getNbLigne()+"x"+niv.getNbColonne()+" pour "+niv.getNbMine()+" mines");
		descNiveauLabel.setFont(ThemeDemineur.getFontText3());
		descNiveauLabel.setForeground(ThemeDemineur.getCouleurEcriture());
		panTitre.setOpaque(false);
		panMeilleursScores.removeAll();
		panMeilleursScores.setLayout(new BorderLayout());
		panMeilleursScores.setFont(ThemeDemineur.getFontText1());

		panTitre.add(descNiveauLabel);//pan portan le libelle du niveau et sa desciptio
		panMeilleursScores.add(panTitre,BorderLayout.NORTH);

		final String[] lesMeilleusScore=demineurModel.getMeilleursScore();
		JList listMeilleusScore = new JList();
		listMeilleusScore.setModel(new AbstractListModel() {
			String[] values =   lesMeilleusScore;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listMeilleusScore.setOpaque(false);
		
		listMeilleusScore.setFont(ThemeDemineur.getFontText1());
		panMeilleursScores.add(new JPanel().add(listMeilleusScore),BorderLayout.CENTER);
		listMeilleusScore.setAlignmentY(0.5f);
		listMeilleusScore.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));


	}

	public Timer getHorloge() {
		return horloge;
	}

	public void setHorloge(Timer horloge) {
		this.horloge = horloge;
	}

	@Override
	public void modelChanged(ModelChangeEvent evt) {

		scoreLabel.setText("score:"+evt.getScore());
		nbMineLabel.setText("Mines"+evt.getMineRestant());
		switch (evt.getEtatPartie()) {
		case ModelChangeEvent.GAGNER:
			horloge.stop();
			imageLabel.setIcon(ThemeDemineur.getEmoticonGagner());
			break;
		case ModelChangeEvent.PERDU:
			horloge.stop();
			imageLabel.setIcon(ThemeDemineur.getEmoticonPerdre());
			break;
		case ModelChangeEvent.PAUSE:
			horloge.stop();
			this.demineurView.setVisible(false);
			this.imageLabel.setIcon(ThemeDemineur.getEmoticonPause());
			break;
		case ModelChangeEvent.ENCOURS:
			if(!this.demineurView.isVisible()){
				this.demineurView.setVisible(true);
				this.imageLabel.setIcon(ThemeDemineur.getEmoticonJouer());
				horloge.restart();
			}
			
			break;
		default:
			break;
		}


	}


}
