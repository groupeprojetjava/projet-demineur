package sn.esp.dic2.projets.game.demineur.view.composant;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
/**
 * redefinit la taille de ml image en fontion de la taille  du conteneur jlabel
 * @author darcia0001
 *
 */
public class ImageResize   extends JButton{
    ImageIcon imageIcon;
    public ImageResize(String text ){
		 super(text);	 
	}
    public ImageResize(ImageIcon icon)
    {
        super();
        this.imageIcon = icon;
    }
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        g.drawImage(imageIcon.getImage(),0,0,getWidth(),getHeight(),this);
    }
}