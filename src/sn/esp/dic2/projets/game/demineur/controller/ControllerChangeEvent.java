package sn.esp.dic2.projets.game.demineur.controller;

import javax.swing.event.ChangeEvent;

public class ControllerChangeEvent extends ChangeEvent {
	public static int MARKED=0;
	public static int REVELED=1;
	private int etat;
	private int ligne;
	private int colonne;

	public ControllerChangeEvent(Object arg0, int ligne, int colonne) {
		super(arg0);
		this.ligne = ligne;
		this.colonne = colonne;
	}

	public int getLigne() {
		return ligne;
	}

	public void setLigne(int ligne) {
		this.ligne = ligne;
	}

	public int getColonne() {
		return colonne;
	}

	public void setColonne(int colonne) {
		this.colonne = colonne;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	@Override
	public String toString() {
		return "ControllerChangeEvent [etat=" + etat + ", ligne=" + ligne
				+ ", colonne=" + colonne + "]";
	}






}
